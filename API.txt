---
API
---

To use this module as an API for a menu display module, you will need to implement one (or more) of the following hooks. You can use drupal_default.module as example code.

--------------------
hook_menu_displayapi
--------------------

Parameters:

$op - Operation being performed. Possible values are 'block settings', 'subject', 'init', 'parent', 'parent top', 'child', or 'final'.

$delta - Optional, should equal 0 by default. The id of the block- 0, 1, 2... (etc).

$mid - Optional, should equal 1 by default. The mid of the current menu item, or, in some circumstances, the $pid.

&$info - Optional, should equal empty array() by default. Information about the menu as generated so far, passed by reference.


Return values:


'block settings': Additional formapi arrays, to be merged into the default settings form.

'subject': HTML code expected, to be displayed as the default title if not overrided on the block configuration page.

'init': HTML code expected, to be entered into the block content before menu processing.

'parent top': HTML code expected, the code for a parent menu item at the top (root) level.

'parent': HTML code expected, the code for a parent menu item NOT at the top (root) level.

'child': HTML code expected, the code for a child menu item.

'final': HTML code expected, to be entered into the block content after menu processing.

--------------------------
hook_menu_displayapi_alter
--------------------------

Can alter the menu block after rendering.

Parameters:

&$block - The block array, with $block['content'] and $block['subject']; all changes except for this hook (and sometimes the addition of a type selection box) have already been made.

Return values:

Should not return anything, but should edit the $block object as passed in by reference.

----
More
----

If you have any requests for additional API features, please post an issue at the project page (http://drupal.org/project/menu_displayapi).