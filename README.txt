------------
Installation
------------

1) Copy this folder to your modules directory.

2) Enable the module at admin/build/modules.

3) Configure settings at admin/settings/menu_displayapi.

4) Enable block and configure block settings at admin/build/block.

---
API
---

See API.txt.

-------
Credits
-------

This module was created by Charlie Gordon (cwgordon7).

Anyone who would like to help maintain this module would be welcomed. Please submit an issue on the project page (http://drupal.org/project/menu_displayapi) if you are interested.